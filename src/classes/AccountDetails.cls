public class AccountDetails {
    public static void m1(){
        Map<Id, Account> acc = new Map<Id, Account>();
        List<Account> a1 = [select id, name from Account limit 10];
        for(Account a: a1){
            acc.put(a.Id,a);
        }
         
    }
    public static void m2(){
        Map<Id, List<Account>> acc = new Map<Id, List<Account>>();
        List<Account> a1 = [select id, name from Account limit 10];
        for(Account a: a1){
            acc.put(a.Id,a1);
        }
        system.debug(acc);
    }
}